package ru.t1.artamonov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.artamonov.tm.api.client.AuthSoapEndpointClient;
import ru.t1.artamonov.tm.api.client.ProjectSoapEndpointClient;
import ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.artamonov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.artamonov.tm.marker.IntegrationCategory;
import ru.t1.artamonov.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @Nullable
    private static final String sessionId = null;

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectRestEndpoint projectEndpoint;

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project4 = new Project(UUID.randomUUID().toString());

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        projectEndpoint.save(project1);
        projectEndpoint.save(project2);
        projectEndpoint.save(project3);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectEndpoint.deleteAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        @NotNull final String expected = project4.getName();
        @Nullable final Project project = projectEndpoint.save(project4);
        Assert.assertNotNull(project);
        @NotNull final String actual = project.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdTest() {
        @NotNull final String expected = project1.getId();
        @Nullable final Project project = projectEndpoint.findById(expected);
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        projectEndpoint.deleteById(id);
        Assert.assertNull(projectEndpoint.findById(id));
    }

}
