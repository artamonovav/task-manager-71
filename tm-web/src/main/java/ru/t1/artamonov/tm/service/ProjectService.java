package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.exception.field.UserIdEmptyException;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        projectRepository.deleteAllByUserId(userId);
    }

    @Transactional
    public void add(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(UserUtil.getUserId());
        projectRepository.save(model);
    }

    @Transactional
    public void addByUserId(@NotNull final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Transactional
    public void update(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(UserUtil.getUserId());
        projectRepository.save(model);
    }

    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Nullable
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Transactional
    public void remove(@Nullable final Project model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepository.deleteById(model.getId());
    }

    @Transactional
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteAllByUserId(userId);
    }


    @Transactional
    public void removeByUserId(@Nullable final Project model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        projectRepository.deleteById(id);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(id, userId);
    }

    @Nullable
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    public Project findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(UserUtil.getUserId(), id);
    }

}
