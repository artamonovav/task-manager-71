package ru.t1.artamonov.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.artamonov.tm.api.repository.ITaskRepository;
import ru.t1.artamonov.tm.config.*;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Task;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class TaskRepositoryTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Task model1 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task model2 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task model3 = new Task(UUID.randomUUID().toString());

    @NotNull
    private final Task model4 = new Task(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        model1.setUserId(UserUtil.getUserId());
        model2.setUserId(UserUtil.getUserId());
        model3.setUserId(UserUtil.getUserId());
        model4.setUserId(UserUtil.getUserId());
        taskRepository.save(model1);
        taskRepository.save(model2);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void contextLoadsTest() {
        Assert.assertNotNull(authenticationManager);
        Assert.assertNotNull(taskRepository);
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        List<Task> tasks = taskRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByIdAndUserId(model1.getId(), UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByIdAndUserId(model1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskRepository.findByIdAndUserId(model1.getId(), UserUtil.getUserId()));
    }

}
